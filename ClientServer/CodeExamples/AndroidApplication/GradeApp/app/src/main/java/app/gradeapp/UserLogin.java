package app.gradeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * @author Kody. Duncan
 * @since 1/13/2016
 *
 * Allows the ability for an user to login.
 */

public class UserLogin extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void onSignInClick(View view) {
        //get copy of link;
        String link = "http://bjmac.hccis.info:8080/cisadmin/rest/useraccess/bjmac_codes,";
        String url = link;

        //get user input
        EditText username = (EditText) findViewById(R.id.username_editText);
        EditText password = (EditText) findViewById(R.id.password_editText);

        String usernameTyped = String.valueOf(username.getText());
        String passwordTyped = String.valueOf(password.getText());

        //add user input as url parameters
        url += usernameTyped + "," + passwordTyped;

        //get login connection
        Connection connection = new Connection(url);
        connection.execute("");
        try {

            //get connection response
            String pass = connection.get();

            //if response 1 allow user access
            if(pass.equalsIgnoreCase("1")) {
                Toast.makeText(this, "Login Worked", Toast.LENGTH_LONG).show();
                Intent mainIntent = new Intent(UserLogin.this, MainActivity.class);
                mainIntent.putExtra("username", usernameTyped);
                startActivity(mainIntent);
                finish();
            } else {
                Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
